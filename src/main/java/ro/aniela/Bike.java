package ro.aniela;

public abstract class Bike {
    public double speed;

    public abstract void speedOf();

    public Bike(double speed) {
        this.speed = speed;
    }

}
