package ro.aniela.abstraction;

public class Exercise4 {

    public static void main(String[] args){

        Person student = new Student("Adi",18, 123,8.5);
        System.out.println(student);
        Person employee= new Employee("cris",19,234, 1234.0);
        System.out.println(employee);
    }
}
