package ro.aniela;

public class Division extends OperationsM {


    public Division(int num1, int num2) {
        super(num1, num2);
    }

    @Override
    public int calculate() {
        return num1 / num2;
    }
}
