package ro.aniela.abstraction;

public class Person {

   private String name;
    protected int age;
    protected int id;

    public Person(String name, int age, int id){
        this.name=name;
        this.age=age;
        this.id=id;

    }
    public String getName(){
        return name;
    }
}
