package ro.aniela;

import java.awt.*;

public class Exercise1 {
    public static void main(String[] args) {
        Romanian ro = new Romanian();
        ro.hello();
        Language l = new French();
        l.hello();
        Language en = new English();
        en.hello();
    }
}
