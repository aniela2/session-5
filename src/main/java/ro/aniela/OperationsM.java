package ro.aniela;

public abstract class OperationsM {
    public int num1;
    public int num2;

    public abstract int calculate();

    public OperationsM(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

}
