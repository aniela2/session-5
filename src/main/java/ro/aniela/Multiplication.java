package ro.aniela;

public class Multiplication extends OperationsM {

    public Multiplication(int num1, int num2) {
        super(num1, num2);
    }

    public int calculate() {
        return num1 * num2;
    }
}
