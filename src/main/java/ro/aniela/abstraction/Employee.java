package ro.aniela.abstraction;

public class Employee extends Person{
    private double salary;

    public Employee(String name, int age, int id , double salary){
        super(name, age, id);
        this.salary=salary;
    }

    public String toString(){
        return " Employee: name " + super.getName() + ", age " + age + "id" + id + "salary" + salary;
    }
}
