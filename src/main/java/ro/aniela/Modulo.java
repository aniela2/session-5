package ro.aniela;

public class Modulo extends OperationsM {
    private int num3;
    public Modulo(int num1, int num2, int num3 ) {
        super(num1, num2);
        this.num3=num3;
    }

    public int calculate() {
        return num1 % num2;
    }
}
