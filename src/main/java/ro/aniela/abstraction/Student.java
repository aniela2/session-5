package ro.aniela.abstraction;

public class Student extends Person {
    private double average;

    public Student(String name, int age, int id, double average) {
        super(name, age, id);
        this.average = average;
    }

    public String toString() {
        return " Student : name " + super.getName() + ", age " + age + ", id " + id + ", average " + average;
    }

}
