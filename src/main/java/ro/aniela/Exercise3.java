package ro.aniela;

public class Exercise3 {

    //Using abstraction, display the maximum speed of a MotorBike and of a MountainBike.
    public static void main(String[] args) {

        Bike mountain = new Mountain(123.0);
        mountain.speedOf();
        Bike motor = new Motor(100.0);
        motor.speedOf();
    }

}
